let text = document.getElementById('text');
let leaf = document.getElementById('leaf');
let hill4 = document.getElementById('hill4');
let hill5 = document.getElementById('hill5');

// const tabs = document.querySelectorAll('[data-tab-target]')
// const tabContents = document.querySelectorAll('[data-tab-content]')

// tabs.forEach(tab => {
//     tab.addEventListener('click' , () => {
//         const target = document.querySelector(tab.dataset.tabTarget)
//         tabContents.forEach(tabContent => {
//             tabContent.classList.remove('active')
//         })
//         tabs.forEach(tab => {
//             tab.classList.remove('active')
//         })
//         tab.classList.add('active')
//         target.classList.add('active')
//     })
// })


window.addEventListener('scroll', () => {
    let value = window.scrollY;

    text.style.marginTop = value * 2.5 + 'px';
    leaf.style.top = value * -1.5 + 'px';
    leaf.style.left = value * 1.5 + 'px';
    hill5.style.left = value * 1.5 + 'px';
    hill4.style.left = value * -1.5 + 'px';
});
